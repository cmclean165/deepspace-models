import tensorflow as tf
import cv2
import numpy as np

import os 

from sklearn.model_selection import train_test_split

from tensorflow import keras
from model.u2net import *

import matplotlib.pyplot as plt

in_path = 'bands'
out_path = 'masks'
model_number = '1'

## get the input and output shapes
in_img = np.load(os.path.join(in_path, '1.npy'))
in_img = cv2.resize(in_img, (256,256))
default_in_shape = in_img.shape
print('Input shape:' ,in_img.shape)

out_img = np.load(os.path.join(out_path, '1.npy'))
out_img = cv2.resize(out_img, (256,256))
print('Output shape:' ,out_img.shape)


## create the u2net model
inputs = keras.Input(shape=default_in_shape)

# rewrite default optimizer
adam = keras.optimizers.Adam(learning_rate=0.0001, beta_1=.9, beta_2=.999, epsilon=1e-08)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath='w_backup.h5', save_weights_only=True, verbose=1)

net = U2NET()
out = net(inputs)

loss_callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)

model = keras.Model(inputs=inputs, outputs=out, name='algo-model')
model.compile(optimizer=adam, loss=bce_loss, metrics=bce_loss)
model.summary()


## import and split training and test data
file_names = os.listdir(in_path)

X, y = [], []

for name in file_names:
    in_img = os.path.join(in_path, name)
    out_img = os.path.join(out_path, name)
    # load and resize image
    img = np.load(in_img)
    img = cv2.resize(img, (256,256))

    # load and resize mask
    mask  = np.load(out_img)
    mask = cv2.resize(mask, (256,256))

    # append normal image
    X.append(img)
    y.append(mask)

    # append horizontal flip
    X.append(cv2.flip(img, 1))
    y.append(cv2.flip(mask, 1))

    if len(X) == 5000:
        break


# normalize the X data with min max scaler
minVal, maxVal = np.nanmin(X), np.nanmax(X)
print("Min:", minVal, "Max:", maxVal)
X = (X - minVal) / (maxVal - minVal)

X_train, _, y_train, _ = train_test_split(X, y, test_size=.1)

## format the data properly
trainx = np.array(X_train)
print('X Shape:', trainx.shape)

trainy = np.array(y_train)
trainy = trainy[:,:,:,None]
print('y Shape:', trainy.shape)


## train the model
history = model.fit(trainx, trainy, batch_size=1, epochs=5000)

img = model.predict(trainx)[0][0]
img = img[:, :, 0]
print(img.shape)
plt.imsave('out.png', img, cmap='gray')

## save the model
# model.save('models/' + model_number)
# ## save the weights
# model.save_weights('weights/' + model_number + '.h5')

