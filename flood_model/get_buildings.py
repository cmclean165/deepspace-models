import os

abd_dir = 'automated-building-detection'
folders = ['abd-input', 'abd-predictions', 'bing-images']

for f in folders:
    remove = 'rm -rf ' + os.path.join(abd_dir, f)
    create = 'mkdir ' + os.path.join(abd_dir, f)
    

    os.system(remove)
    os.system(create)

os.system('download-images --aoi aoi.geojson --output automated-building-detection/bing-images')

os.system('images-to-abd --images automated-building-detection/bing-images/images --output automated-building-detection/abd-input')

os.system('abd predict --config automated-building-detection/input/config.toml --dataset automated-building-detection/abd-input --cover automated-building-detection/abd-input/cover.csv --checkpoint automated-building-detection/input/neat-fullxview-epoch75.pth --out automated-building-detection/abd-predictions --metatiles --keep_borders')

os.system('abd vectorize --config automated-building-detection/input/config.toml --type Building --masks automated-building-detection/abd-predictions --out automated-building-detection/abd-predictions/buildings.geojson')

os.system('filter-buildings --data automated-building-detection/abd-predictions/buildings.geojson --dest automated-building-detection/abd-predictions/buildings-clean.geojson')
