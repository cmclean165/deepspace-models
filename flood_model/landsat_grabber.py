from select import select
from pystac_client import Client
import numpy as np
import json
import cv2

import rasterio.features
import stackstac
import planetary_computer as pc
from urllib.request import urlopen

import matplotlib.pyplot as plt

class planetary_computer:
    def __init__(self, sat='landsat'):
        self.sat=sat

        if self.sat == "landsat":
            self.p_col = "landsat-8-c2-l2"
            self.sat_bands = ["SR_B2", "SR_B3", "SR_B4", "SR_B5", "SR_B6"]

        elif self.sat == 'sentinel':
            self.p_col = "sentinel-2-l2a"
            self.sat_bands = ["B02", "B03", "B04", "B08", "B11"]

        self.data = None
        self.mtl = []
        self.computed_data = {}
        self.coefficients = []

        # min max values for scaling
        self.train_min = 0.0
        self.train_max = 65454.0

        self.default_in_shape = (256, 256, 3)
        self.original_shape = None

    def _r_coeffs_from_href(self, href):
        ML, AL = None, None
        first = 'RADIANCE_MULT_BAND_'
        second = 'RADIANCE_ADD_BAND_'
        coeffs = {}

        for loc, name in enumerate(self.data.band.to_numpy()):
            one = first + str(loc+1)
            two = second + str(loc+1)

            for line in urlopen(href):
                text = line.decode('utf-8')

                if one in text:
                    ML = text.split()[2]

                if two in text:
                    AL = text.split()[2]    

            coeffs[name] = [float(ML), float(AL)]

        return coeffs

    def _get_r_coeffs(self, items):
        for item in items:
            #get ML and AL coefficients
            temp = json.dumps(item.assets['MTL.txt'].to_dict(), indent=2)
            temp2 = json.loads(temp)
            self.coefficients.append(self._r_coeffs_from_href(pc.sign(temp2['href'])))

    def _read_band_data(self, asset_num, band):
        return self.data[asset_num].sel(band=band).to_numpy()

    def _calculate_toa(self, asset_num, band):
        ML = self.coefficients[asset_num][band][0]
        AL = self.coefficients[asset_num][band][1]
        toa = ( self._read_band_data(asset_num, band) * ML ) + AL
        return toa

    def get_aoi_data(self, aoi, time_range, cloud_cover=15, resolution=100):
        bbox = rasterio.features.bounds(aoi)

        # open planetary computer client
        stac = Client.open("https://planetarycomputer.microsoft.com/api/stac/v1")

        # search database for matching results
        search = stac.search(
            bbox=bbox,
            datetime=time_range,
            collections=[self.p_col],
            limit=500,  # fetch items in batches of 500
            query={"eo:cloud_cover": {"lt": cloud_cover}},
        )

        items = list(search.get_items())

        selected_items = sorted(items, key=lambda item: item.properties["datetime"]) 

        # sign the items before accessing them
        signed_items = [pc.sign(item).to_dict() for item in selected_items]

        # create stackstac data
        self.data = (
            stackstac.stack(
                signed_items,
                assets=self.sat_bands, # bands to grab
                chunksize=4096,
                resolution=resolution,
                bounds_latlon=bbox,
                epsg=3857
            )
        )

        # make the common name the key for each data band

        self.data = self.data.assign_coords(band=self.data.common_name.fillna(self.data.band).rename("band"))

    def MNDWI_mask(self, asset_num=0, thresh=0.0):

        swir_toa = self._read_band_data(asset_num, 'swir16')
        green_toa = self._read_band_data(asset_num, 'green')

        MNDWI = (green_toa - swir_toa) / (green_toa + swir_toa)
        # MNDWI[np.where(MNDWI <= thresh)] = 0 # water
        # MNDWI[np.where(MNDWI > thresh)] = 1 # land

        return MNDWI

    def FAI_algo(self, asset_num=0, thresh=-0.002, nan_thresh=0.25):

        red_toa = self._read_band_data(asset_num, 'red')
        swir_toa = self._read_band_data(asset_num, 'swir16')

        if self.sat == 'landsat':
            nir_toa = self._read_band_data(asset_num, 'nir08')
        elif self.sat == 'sentinel':
            nir_toa = self._read_band_data(asset_num, 'nir')

        real_ratio = np.count_nonzero(~np.isnan(red_toa)) / (red_toa.shape[0] * red_toa.shape[1])
        if real_ratio < nan_thresh:
            # print('Did not meet land thresh:', real_ratio)
            return None

        prime = red_toa + (swir_toa - red_toa) * ((860 - 650) / (1600 - 650))

        FAI = nir_toa - prime

        # mask where only the algal bloom occurs
        FAI[np.where(FAI > -0.002)] = np.nan


        if not np.isnan(FAI).all():
            max, min = np.nanmax(FAI), np.nanmin(FAI)
            scaled_FAI = (FAI - min) / (max - min)
        else:
            scaled_FAI = FAI

        scaled_FAI[np.isnan(scaled_FAI)] = 1.0
        
        #invert the FAI... areas with more algal bloom are brighter
        inverted_FAI = 1.0 - scaled_FAI

        return inverted_FAI

    def make_composite_rgb(self, asset_num=0):
        # select only rgb bands for a given picture
        rgb = self.data[asset_num].sel(band=["red", "green", "blue"])
        # convert to numpy and make correct stack
        image = rgb.to_numpy()
        image = np.dstack([array for array in image])

        # rescale to 0-255 (int) range
        max, min = np.nanmax(image), np.nanmin(image)
        scaled = (image - min) / (max - min)
        scaled = scaled * 255

        scaled[np.where(scaled > 255)] = 255

        return scaled.astype(int)

    def data_stack(self, asset_num=0):
        # select only rgb bands for a given picture
        rgb = self.data[asset_num].sel(band=["blue", "green", "red", "nir", "swir16"])
        # convert to numpy and make correct stack
        image = rgb.to_numpy()
        image = np.dstack([array for array in image])

        # rescale to 0-255 (int) range
        max, min = np.nanmax(image), np.nanmin(image)
        scaled = (image - min) / (max - min)

        scaled[np.where(scaled > 1)] = 1

        return scaled.astype(float)

    def get_bounds(self, asset_num):
        bounds = stackstac.array_bounds(self.data[asset_num], 4326)

        return bounds
        

    def format_for_map(self, img, thresh = 0.3):

        # convert back to original img size
        resized = cv2.resize(img, (self.original_shape[1], self.original_shape[0]))

        # threshold algal content
        resized[np.where(resized < thresh)] = 0

        # convert prediction to heat map with depth of 3
        cmap = plt.get_cmap('jet')
        rgba_img = cmap(resized)

        # make parts of the img transparent
        rgba_img[:,:,3][np.where(resized < thresh)] = 0
        rgba_img = (rgba_img * 255).astype('uint8')

        return rgba_img


    def format_for_model(self, num, sat):

        selected_data = self.data[num]

        # pull out the red, nir, and swir bands
        if sat == 'landsat':
            rns = selected_data.sel(band=["red", "nir08", "swir16"]).to_numpy()  
        elif sat == 'sentinel':
            rns = selected_data.sel(band=["red", "nir", "swir16"]).to_numpy()  

        # nromalize the rns data and set np.nan to 0
        normalized = []
        for array in rns:
            self.in_shape = array.shape
            array = (array - self.train_min) / (self.train_max - self.train_min)
            array[np.isnan(array)] = 0
            normalized.append(array)

        # restack the band data for NxNx3 image format
        normalized = np.dstack([array for array in normalized])
        self.original_shape = normalized.shape
        
        resized = cv2.resize(normalized, (self.default_in_shape[0], self.default_in_shape[1]))
        # resize to input shape

        out = resized[None, :, :, :].tolist()

        return out
        