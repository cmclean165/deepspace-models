import os
from functools import partial

import geopandas as gpd
import matplotlib.pyplot as plt
import pyproj
from shapely.geometry import box
from shapely.ops import cascaded_union, transform

import geojson
import requests
import json
import cv2

from landsat_grabber import planetary_computer


crs = 'epsg:4326'

satelite_to_use = 'sentinel'

# define a landsat data grabber object
sat = planetary_computer(satelite_to_use)

## time to get some data

# first define time range and cloud cover threshold
time_range = "2019-03-21/2020-03-21"
cloud_cover = 10

# use geojson.io to get coordinates from a polygon for testing
area_of_interest = geojson.load(open("aoi.geojson"))

# DOWNLOAD SAT DATA
# finally import landsat data
sat.get_aoi_data(area_of_interest, time_range, cloud_cover=cloud_cover, resolution=10)

# show the data
sat.data

num = 0

rgb = sat.make_composite_rgb(num)

h, w, _ = rgb.shape

# transform crs
crs_project = partial(
    pyproj.transform,
    pyproj.Proj(init='epsg:3857'), # source coordinate system
    pyproj.Proj(init='epsg:4326') # destination coordinate system
)

bbox = sat.data[0].spec.bounds

# read bounds from dataset and transform to wgs84
osm_poly = transform(crs_project, box(bbox[0], bbox[1], bbox[2], bbox[3]))


# DWONLOAD THE HAZARD AREA MASK
url = 'https://hazards.fema.gov/gis/nfhl/rest/services/public/NFHLWMS/MapServer/28/query?where=&text=&objectIds=&time=&geometry={0}%2C+{1}%2C+{2}%2C+{3}&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelContains&distance=&units=esriSRUnit_Foot&relationParam=&outFields=&returnGeometry=true&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=geojson'
url = url.format(*tuple(osm_poly.bounds))

val = requests.get(url)
j = json.dumps(val.json())

hazard = gpd.read_file(j)
hazard.to_crs(crs='epsg:3857', inplace=True)

# save mask image 
hazard.plot()
plt.axis('off')
plt.savefig('tmp.png',bbox_inches='tight')

mask = cv2.imr

mask = cv2.imread('tmp.png', cv2.IMREAD())